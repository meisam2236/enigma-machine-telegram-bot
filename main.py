import logging
import telegram
from telegram.error import NetworkError, Unauthorized
from time import sleep
import random


update_id = None
# enter your token here:
token = ''
alphabet = 'abcdefghijklmnopqrstuvwxyz '

def init_rotor():
    random.seed(27)
    rotor1 = list(alphabet)
    random.shuffle(rotor1)
    rotor1 = ''.join(rotor1)
    random.seed(28)
    rotor2 = list(alphabet)
    random.shuffle(rotor2)
    rotor2 = ''.join(rotor2)
    random.seed(29)
    rotor3 = list(alphabet)
    random.shuffle(rotor3)
    rotor3 = ''.join(rotor3)
    return rotor1, rotor2, rotor3

def rotate_rotors(rotors_state, r1, r2, r3):
    r1 = r1[1:] + r1[0]
    if rotors_state % 26:
        r2 = r2[1:] + r2[0]
    if rotors_state % (26*26):
        r3 = r3[1:] + r3[0]
    return r1, r2, r3
    
def reflector(c):
    element_index = alphabet.find(c)
    return alphabet[len(alphabet) - element_index - 1]

def enigma_one_character(c, r1, r2, r3):
    c1 = r1[alphabet.find(c)]
    c2 = r2[alphabet.find(c1)]
    c3 = r3[alphabet.find(c2)]
    reflected = reflector(c3)
    c3 = alphabet[r3.find(reflected)]
    c2 = alphabet[r2.find(c3)]
    c1 = alphabet[r1.find(c2)]
    return c1

def cipher(text, r1, r2, r3):
    rotors_state = 0
    cipher_text = ''
    for c in text:
        rotors_state += 1
        cipher_text += enigma_one_character(c, r1, r2, r3)
        r1, r2, r3 = rotate_rotors(rotors_state, r1, r2, r3)
    return cipher_text

def main():
    global update_id
    bot = telegram.Bot(token)
    try:
        update_id = bot.getUpdates()[0].update_id
    except IndexError:
        update_id = None

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    while True:
        try:
            echo(bot)
        except NetworkError:
            sleep(1)
        except Unauthorized:
            update_id += 1

def echo(bot):
    global update_id
    for update in bot.getUpdates(offset=update_id, timeout=10):
        chat_id = update.message.chat_id
        update_id = update.update_id + 1

        if update.message:
            r1, r2, r3 = init_rotor()
            incomming_message = update.message.text
            m = cipher(incomming_message, r1, r2, r3)
            bot.sendMessage(chat_id=chat_id, text=m)

if __name__ == '__main__':
    main()